<?php
session_start(); ?>
<?php 
// Check login
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['username'])) {
	if($_POST['username'] == "admin" && $_POST['password'] == "admin@123") {
		$_SESSION['admin'] = "admin";
	}else{
		echo "<script>alert('Wrong username/password')</script>";
	}
}

// Publish categories
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['techCat'])) {
	file_put_contents('tech.json', json_encode($_POST['techCat']));
	file_put_contents('business.json', json_encode($_POST['businessCat']));

	echo "<script>alert('All categories published')</script>";
}

// Logout
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['logout'])) {
	session_destroy();

	header('Location: admin.php');
}
 ?>
<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BIG</title>
    <link rel="icon" type="image/png" href="img/logo.png" />
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/component.css" />
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>

<body>
	<div class="container">
	<?php
	if(isset($_SESSION['admin'])) {
		$businessCat = json_decode(file_get_contents('business.json'));
		$techCat = json_decode(file_get_contents('tech.json'));
		// Show CMS ?>
		
		<div class="row">
			<div class="col-md-12">
				<img src="img/logo_yellow.png" style="margin-top: 20px;" alt="BIG">
				<form action="admin.php" method="post">
					<input type="hidden" name="logout" value="true">
					<input style="position: absolute; right: 0; top: 20px;" type="submit" value="Logout" class="btn btn-default">
				</form>
			</div>
		</div>
		<hr>

		<form action="admin.php" method="post">
			<button type="submit" class="btn btn-warning">Publish all categories</button>
			<small>Press "Publish all" to save the data</small>
			<div class="row">
				<div class="col-md-6">
					<h2>Business categories</h2>
					<table class="table">
						<tr>
							<td><input type="text" id="newBusinessCat" placeholder="Type in category name then click Add" class="form-control"></td>
							<td style="width: 20px;"><span class="btn btn-success btn-add" id="addBusinessCat">Add</span></td>
						</tr>
					</table>

					<table class="table" id="businessCatWrap">
						<?php foreach ($businessCat as $item) { ?>
							<tr>
								<td><input type="text" name="businessCat[]" value="<?php echo $item; ?>" class="form-control"></td>
								<td style="width: 20px;"><span class="btn btn-danger btn-delete">Delete</span></td>
							</tr>
						<?php } ?>
					</table>
				</div>
				<div class="col-md-6">
					<h2>Tech categories</h2>
					<table class="table">
						<tr>
							<td><input type="text" id="newTechCat" placeholder="Type in category name then click Add" class="form-control"></td>
							<td style="width: 20px;"><span class="btn btn-success btn-add" id="addTechCat">Add</span></td>
						</tr>
					</table>
					<table class="table" id="techCatWrap">
						<?php foreach ($techCat as $item) { ?>
							<tr>
								<td><input type="text" name="techCat[]" value="<?php echo $item; ?>" class="form-control"></td>
								<td style="width: 20px;"><span class="btn btn-danger btn-delete">Delete</span></td>
							</tr>
						<?php } ?>
					</table>
				</div>
			</div>
		</form>

	<?php
	}else{
		// Show login form ?>
		
		<div class="col-md-4 col-md-offset-4">
			<h2>BIG Login</h2>
			<form action="admin.php" method="post">
				<div class="form-group">
					<input type="text" name="username" placeholder="Username" class="form-control">
				</div>
				<div class="form-group">
					<input type="password" name="password" placeholder="Password" class="form-control">
				</div>
				<div class="form-group">
					<input type="submit" value="Login" class="btn btn-primary form-control">
				</div>
			</form>
		</div>
	<?php } ?>
	</div>
    <!-- Scripts -->
    <script src="js/jquery.js"></script>
    <script src="js/main.js"></script>
</body>

</html>

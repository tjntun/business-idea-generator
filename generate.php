<?php 
$businessCat = json_decode(file_get_contents('business.json'));
$techCat = json_decode(file_get_contents('tech.json'));

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	$newBusiness = $businessCat[mt_rand(0, count($businessCat)-1)];
	$newTech = $techCat[mt_rand(0, count($techCat)-1)];

	echo json_encode(array('newBusiness' => $newBusiness, 'newTech' => $newTech));
}
?>
jQuery(document).ready(function($) {
	$('.toggle').click(function(){
		$(this).toggleClass('active');
	});

	$('#btn-generate').click(function(){
		$('body,html').animate({scrollTop: $('#generator-wrapper').offset().top},1000);

		$('#generator-wrapper').addClass('hide');

		setTimeout(function(){
			generate();
		},1500);
		
	});

	generate();

	function generate () {
		$.ajax({
			url: 'generate.php',
			type: 'POST',
		})
		.done(function(res) {
			res = JSON.parse(res);
			if(!$('#tech-lock').hasClass('active')) {
				$('#tech-sector').text(res.newTech);
			}
			if(!$('#business-lock').hasClass('active')) {
				$('#business-sector').text(res.newBusiness);
			}
		})
		.fail(function() {
			alert('Sorry! Please try again.');
		})
		.always(function() {
			$('#generator-wrapper').removeClass('hide');
		});
	}

	$('#menu-toggle').click(function(ev){
		ev.stopPropagation();
		$('.effect-airbnb').addClass('animate modalview');
	});

	$('body').click(function(){
		if($('.effect-airbnb').hasClass('animate')){
			$('.effect-airbnb').removeClass('animate modalview');
		}
	});

	// CMS
	$('body').delegate('.btn-delete', 'click', function(event) {
		$(this).parents('tr').remove();
	});

	$('#addBusinessCat').click(function(event) {
		var newBusiness = $('#newBusinessCat').val();

		if (newBusiness == '') {
			alert('Please fill in category name');
		}else{
			var item = '';
			item += '<tr>';
			item += '	<td><input type="text" name="businessCat[]" value="'+newBusiness+'" class="form-control"></td>';
			item += '	<td style="width: 20px;"><span class="btn btn-danger btn-delete">Delete</span></td>';
			item += '</tr>';
			$('#businessCatWrap').prepend(item);
		}
	});

	$('#addTechCat').click(function(event) {
		var newTechCat = $('#newTechCat').val();

		if (newTechCat == '') {
			alert('Please fill in category name');
		}else{
			var item = '';
			item += '<tr>';
			item += '	<td><input type="text" name="techCat[]" value="'+newTechCat+'" class="form-control"></td>';
			item += '	<td style="width: 20px;"><span class="btn btn-danger btn-delete">Delete</span></td>';
			item += '</tr>';
			$('#techCatWrap').prepend(item);
		}
	});
});